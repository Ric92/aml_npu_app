#include <iostream>
#include <fstream>

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>

#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <dirent.h>
#include <queue>
#include <fcntl.h>
#include <string.h>
#include <linux/fb.h>
#include <linux/kd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <VX/vx.h>
#include <VX/vxu.h>
#include <VX/vx_api.h>
#include <VX/vx_khr_cnn.h>
#include <semaphore.h>
#include <sys/time.h>
#include <sched.h>
#include <linux/videodev2.h>
#include <poll.h>
#include <semaphore.h>
#include <sys/resource.h>

#include "nn_detect.h"
#include "nn_detect_utils.h"


#define FASTCOM_HAS_OPENCV 1
#include <fastcom/fastcom.h>

using namespace std;
using namespace cv;

#define BUFFER_COUNT 6
#define MAX_HEIGHT  480
#define MAX_WIDTH   640

typedef struct __video_buffer
{
	void *start;
	size_t length;

}video_buf_t;

struct  Frame
{   
	size_t length;
	int height;
	int width;
	unsigned char data[MAX_HEIGHT * MAX_WIDTH * 3];
} frame;


const char *xcmd="echo 1080p60hz > /sys/class/display/mode;\
fbset -fb /dev/fb0 -g 1920 1080 1920 2160 32;\
echo 1 > /sys/class/graphics/fb0/freescale_mode;\
echo 0 0 1919 1079 >  /sys/class/graphics/fb0/window_axis;\
echo 0 0 1919 1079 > /sys/class/graphics/fb0/free_scale_axis;\
echo 0x10001 > /sys/class/graphics/fb0/free_scale;\
echo 0 > /sys/class/graphics/fb0/blank;";

static int fbfd = 0;
static struct fb_var_screeninfo vinfo;
static struct fb_fix_screeninfo finfo;
static long int screensize = 0;
char *fbp;
int opencv_ok = 0;
static char *video_device = NULL;
/* --------------------------- */
bool recorDataset = false;
std::string g_storeFolder;
unsigned int g_storeIndex = 0;
fastcom::ImagePublisher publisher(8888);

struct features {
    float timestamp;
    int height;
    int width;
    int u1;
    int v1;
    int u2;
    int v2;
    int u3;
    int v3;
    int u4;
    int v4;
};

fastcom::Publisher<features> boundingBoxPub(7777);
features newFeature;
bool g_newPerson = false;

/* --------------------------- */

pthread_mutex_t mutex4q;

unsigned char *displaybuf;
int g_nn_height, g_nn_width, g_nn_channel;
det_model_type g_model_type;
static unsigned int tmpVal;

#define _CHECK_STATUS_(status, stat, lbl) do {\
	if (status != stat) \
	{ \
		cout << "_CHECK_STATUS_ File" << __FUNCTION__ << __LINE__ <<endl; \
	}\
	goto lbl; \
}while(0)

int minmax(int min, int v, int max)
{
	return (v < min) ? min : (max < v) ? max : v;
}

static void draw_results(IplImage *pImg, DetectResult resultData, int img_width, int img_height, det_model_type type)
{
	int i = 0;
	float left, right, top, bottom;
	CvFont font;
    cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 1, 1,0,3,8);

	cout << "\nresultData.detect_num=" << resultData.detect_num <<endl;
//	cout << "result type is " << resultData.point[i].type << endl;

	for (i = 0; i < resultData.detect_num; i++) {

		bool isPerson = false;
		if (resultData.result_name[i].lable_id == 0)
			isPerson = true;
		if(isPerson){
			left =  resultData.point[i].point.rectPoint.left*img_width;
			right = resultData.point[i].point.rectPoint.right*img_width;
			top = resultData.point[i].point.rectPoint.top*img_height;
			bottom = resultData.point[i].point.rectPoint.bottom*img_height;
			newFeature.height = img_height;
			newFeature.width = img_width;
			newFeature.u1 = left;
			newFeature.v1 = top;
			newFeature.u2 = right;
			newFeature.v2 = top;
			newFeature.u3 = left;
			newFeature.v3 = bottom;
			newFeature.u4 = right;
			newFeature.v4 = bottom;
			g_newPerson = true;
			
	//		cout << "i:" <<resultData.detect_num <<" left:" << left <<" right:" << right << " top:" << top << " bottom:" << bottom <<endl;
			CvPoint pt1;
			CvPoint pt2;
			pt1=cvPoint(left,top);
			pt2=cvPoint(right, bottom);

			cvRectangle(pImg,pt1,pt2,CV_RGB(255,10,10),3,4,0);
			switch (type) {
				case DET_YOLOFACE_V2:
				break;
				case DET_MTCNN_V1:
				{
					int j = 0;
					cv::Mat testImage;
					testImage = cv::cvarrToMat(pImg);
					for (j = 0; j < 5; j ++) {
						cv::circle(testImage, cv::Point(resultData.point[i].tpts.floatX[j]*img_width, resultData.point[i].tpts.floatY[j]*img_height), 2, cv::Scalar(0, 255, 255), 2);
					}
					break;
				}
				case DET_YOLO_V2:
				case DET_YOLO_V3:
				case DET_YOLO_TINY:
				{
					if (top < 50) {
						top = 50;
						left +=10;
	//					cout << "left:" << left << " top-10:" << top-10 <<endl;
					}
					cvPutText(pImg, resultData.result_name[i].lable_name, cvPoint(left,top-10), &font, CV_RGB(0,255,0));
					break;
				}
				default:
				break;
			}
		}
	}

		{
			// Mat rgbImage;
			// cv::Mat sourceFrame111 = cvarrToMat(pImg);
			// cvtColor(sourceFrame111, rgbImage, CV_BGR2RGB);
			// IplImage test = IplImage(rgbImage);

			// memcpy(fbp+MAX_HEIGHT*MAX_WIDTH*3,pImg->imageData,MAX_HEIGHT*MAX_WIDTH*3);
			// vinfo.activate = FB_ACTIVATE_NOW;
			// vinfo.vmode &= ~FB_VMODE_YWRAP;
			// vinfo.yoffset = 1080;
			// ioctl(fbfd, FBIOPAN_DISPLAY, &vinfo);
		}
}


int run_detect_model(det_model_type type)
{
	int ret = 0;
	int nn_height, nn_width, nn_channel, img_width, img_height;
	DetectResult resultData;

	det_set_log_config(DET_DEBUG_LEVEL_WARN,DET_LOG_TERMINAL);
	cout << "det_set_log_config Debug" <<endl;

	//prepare model
	ret = det_set_model(type);
	if (ret) {
		cout << "det_set_model fail. ret=" << ret <<endl;
		return ret;
	}
	cout << "det_set_model success!!" << endl;

	ret = det_get_model_size(type, &nn_width, &nn_height, &nn_channel);
	if (ret) {
		cout << "det_get_model_size fail" <<endl;
		return ret;
	}

	cout << "\nmodel.width:" << nn_width <<endl;
	cout << "model.height:" << nn_height <<endl;
	cout << "model.channel:" << nn_channel << "\n" <<endl;

	g_nn_width = nn_width;
	g_nn_height = nn_height;
	g_nn_channel = nn_channel;

	return ret;
}


static int init_fb(void)
{
	long int i;

	printf("init_fb...\n");

    // Open the file for reading and writing
    fbfd = open("/dev/fb0", O_RDWR);
    if (!fbfd)
    {
        printf("Error: cannot open framebuffer device.\n");
        exit(1);
    }

    // Get fixed screen information
    if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo))
    {
        printf("Error reading fixed information.\n");
        exit(2);
    }

    // Get variable screen information
    if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo))
    {
        printf("Error reading variable information.\n");
        exit(3);
    }
    printf("%dx%d, %dbpp\n", vinfo.xres, vinfo.yres, vinfo.bits_per_pixel );
/*============add for display BGR begin================,for imx290,reverse color*/
#if 1
	vinfo.red.offset = 0;
	vinfo.red.length = 0;
	vinfo.red.msb_right = 0;

	vinfo.green.offset = 0;
	vinfo.green.length = 0;
	vinfo.green.msb_right = 0;

	vinfo.blue.offset = 0;
	vinfo.blue.length = 0;
	vinfo.blue.msb_right = 0;	

	vinfo.transp.offset = 0;
	vinfo.transp.length = 0;
	vinfo.transp.msb_right = 0;	
	vinfo.nonstd = 0;
	vinfo.bits_per_pixel = 24;
#else
	vinfo.red.offset = 0;
	vinfo.green.offset = 8;
	vinfo.blue.offset = 16;
#endif

	//vinfo.activate = FB_ACTIVATE_NOW;   //zxw
	//vinfo.vmode &= ~FB_VMODE_YWRAP;
	if (ioctl(fbfd, FBIOPUT_VSCREENINFO, &vinfo) == -1) {
        printf("Error reading variable information\n");
    }
/*============add for display BGR end ================*/	
    // Figure out the size of the screen in bytes
    screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 4;  //8 to 4

    // Map the device to memory
    fbp = (char *)mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED,
                       fbfd, 0);

    if (fbp == NULL)
    {
        printf("Error: failed to map framebuffer device to memory.\n");
        exit(4);
    }
	return 0;
}


static void *thread_func(void *x)
{
    IplImage *frame2process = NULL,*frameclone = NULL;
	cv::Mat frame_in(MAX_WIDTH, MAX_HEIGHT, CV_8UC3);
    int width , height; 
    bool bFrameReady = false;
    int i = 0,ret = 0;
    FILE *tfd;
	char gst_str[256];
    struct timeval tmsStart, tmsEnd;
	DetectResult resultData;

	int video_width, video_height;

	cv::Mat yolo_v2Image(g_nn_width, g_nn_height, CV_8UC1);

	sprintf(gst_str, "v4l2src device=%s ! image/jpeg,width=1920,height=1080,framerate=30/1 ! jpegdec ! videoconvert ! video/x-raw, format=(string)BGR ! appsink", video_device);
	cv::VideoCapture cap(video_device);
	cap.set(CV_CAP_PROP_FRAME_WIDTH,MAX_WIDTH);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT,MAX_HEIGHT);
	cap.set(CV_CAP_PROP_FPS,120);
	cap.set(CV_CAP_PROP_FOURCC, CV_FOURCC('M','J','P','G'));
	auto start_time = std::chrono::high_resolution_clock::now();
	std::ofstream dataset("Timestamp.txt");
	cv::Size S = cv::Size((int) MAX_WIDTH, (int) MAX_HEIGHT);
	cv::VideoWriter outputVideo("out.avi", CV_FOURCC('M','J','P','G'),30,S);

	if (!cap.isOpened()) {
		cout << "capture device failed to open!" << endl;

		goto out;
	}

	cout << "open video successfully!" << endl;

	video_width = cap.get(CV_CAP_PROP_FRAME_WIDTH);
	video_height = cap.get(CV_CAP_PROP_FRAME_HEIGHT);

	printf("video_width: %d, video_height: %d\n", video_width, video_height);

    setpriority(PRIO_PROCESS, pthread_self(), -15);

	

   while (true) {
        pthread_mutex_lock(&mutex4q);

		if(opencv_ok == 1)
        {
    		if (!cap.read(frame_in)) {
				cout<<"Capture read error"<<std::endl;
				break;
			}
        }
        else
        {
            if(frame2process == NULL)
                frame2process = cvCreateImage(cvSize(MAX_HEIGHT, MAX_WIDTH), IPL_DEPTH_8U, 3);
            if (frame2process == NULL)
            {
                pthread_mutex_unlock(&mutex4q);
                usleep(100000);
                //printf("can't load temp bmp in thread to parse\n");
                continue;
                }
            if(frame2process->width != MAX_HEIGHT)
            {
                printf("read image not MAX_HEIGHT width\n");
                pthread_mutex_unlock(&mutex4q);
                continue;
            }
            printf("prepare 1080p image ok\n");
            opencv_ok = 1;   //zxw
        }
        pthread_mutex_unlock(&mutex4q);


        gettimeofday(&tmsStart, 0);

		*frame2process = IplImage(frame_in);


		cv::Mat sourceFrame = cvarrToMat(frame2process);
		// cv::Size s = sourceFrame.size();
		// cout << "Frame height: " << s.height << " width: " << s.width << std::endl;
		
        cv::resize(sourceFrame, yolo_v2Image, yolo_v2Image.size());
		gettimeofday(&tmsEnd, 0);
		tmpVal = 1000 * (tmsEnd.tv_sec - tmsStart.tv_sec) + (tmsEnd.tv_usec - tmsStart.tv_usec) / 1000;

		gettimeofday(&tmsStart, 0);
		int img_width = sourceFrame.cols;
		int img_height = sourceFrame.rows;

		input_image_t image;
		image.data      = yolo_v2Image.data;
		image.width     = yolo_v2Image.cols;
		image.height    = yolo_v2Image.rows;
		image.channel   = yolo_v2Image.channels();
		image.pixel_format = PIX_FMT_RGB888;

		// cout << "Det_set_input START" << endl;
		ret = det_set_input(image, g_model_type);
		if (ret) {
			cout << "det_set_input fail. ret=" << ret << endl;
			det_release_model(g_model_type);
			goto out;
		}
		// cout << "Det_set_input END" << endl;

		// cout << "Det_get_result START" << endl;
		ret = det_get_result(&resultData, g_model_type);
		if (ret) {
			cout << "det_get_result fail. ret=" << ret << endl;
			det_release_model(g_model_type);
			goto out;
		}
		// cout << "Det_get_result END" << endl;

		
		draw_results(frame2process, resultData, img_width, img_height, g_model_type);
        // cout << "Draw results ok" << std::endl;
		gettimeofday(&tmsEnd, 0);
        tmpVal = 1000 * (tmsEnd.tv_sec - tmsStart.tv_sec) + (tmsEnd.tv_usec - tmsStart.tv_usec) / 1000;
        // if(tmpVal < 56)
        printf("FPS:%d\n",1000/(tmpVal+8));
        sourceFrame.release();

		cv::Mat labeledImage;
		labeledImage = cv::cvarrToMat(frame2process);

		// record labeled image dataset
		if (labeledImage.rows != 0 && recorDataset)
            cv::imwrite(g_storeFolder + "/detection_" + std::to_string(g_storeIndex) + ".jpeg", labeledImage);

		// Send image using fastcom
		publisher.publish(labeledImage, 30);
		sourceFrame.release();
		auto elapsed_time = std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::high_resolution_clock::now() - start_time).count();
		std::cout << "Time: " << elapsed_time << std::endl;
		dataset << elapsed_time << std::endl;
		g_storeIndex++;

		if(g_newPerson){
			// Publish bounding box
			newFeature.timestamp = elapsed_time;
			boundingBoxPub.publish(newFeature);
			g_newPerson = false;
		}
		// outputVideo.write(labeledImage);
    }
out:
   cap.release();
	printf("thread_func exit\n");
}

int main(int argc, char** argv)
{
	int i;
	pthread_t tid[2];
	det_model_type type;
	if (argc < 3) {
		cout << "VIGUS TEAM input param error" <<endl;
		cout << "Usage: " << argv[0] << " <video device> <type>"<<endl;
		cout << "       video device:"<<endl;
		cout << "       /dev/videoX\n"<<endl;
		cout << "       type: " <<endl;
		cout << "       0 - Yoloface"<<endl;
		cout << "       1 - YoloV2"<<endl;
		cout << "       2 - YoloV3"<<endl;
		cout << "       3 - YoloTiny" << endl;
		return -1;
	}

	system(xcmd);
	// init_fb();

	video_device = argv[1];
	type = (det_model_type)atoi(argv[2]);
	if(!strcmp(argv[3], "yes")){
		recorDataset = true;
		// Create storage folder
		g_storeFolder = "dataset_" + std::to_string(time(NULL));
		mkdir(g_storeFolder.c_str(), 0700);
		std::cout << "Created storagefolder: " << g_storeFolder << std::endl;
	}

	g_model_type = type;
	run_detect_model(type);

	pthread_mutex_init(&mutex4q,NULL);

	if (0 != pthread_create(&tid[0], NULL, thread_func, NULL)) {
		fprintf(stderr, "Couldn't create thread func\n");
		return -1;
	}

	while(1)
	{
		for(i=0; i<sizeof(tid)/sizeof(tid[0]); i++)
		{
			pthread_join(tid[i], NULL);
		}
		sleep(1);
	}


	return 0;
}
